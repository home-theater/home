# Home cinema

This is a semi-automated installer for Kodi, some (custom) addons, and some custom web services.
This also does a lot of configuration.

The end result is a fully-featured home theater.

The system comes in 3 variants:

## Option 1: Download a preset

Requirements:

- A device: Raspberry Pi, Desktop, Laptop or Android phone.
- Debrid account (for movies & series).
- (Optional) Trakt account.
- (Optional) Accounts for local regional media services.

Features:

- All often-viewed movies & series instantly viewable.
- Obscure (colored gray) movies & series need to be downloaded first, the download progress is only visible from your debrid website.
- Watch Youtube, easily cast Youtube from your phone (Only for tv-connected installs).
- Watch local regional content (For example in Flanders we can watch most channels live and access most content aired in the last ~2 years in Kodi).
- Regular TV Remotes fully supported.
- Ability to view your own pictures/videos/music in kodi from local shares, cloud drives or files on your phone.

Installing goes like this:

- Install kodi on your device.
- Download some zip folder from here.
- Depending on your device, copy the contents of the zip somewhere, or load the zip as a backup.
- Login to your accounts.


## Option 2: Manually do a basic install

This has the same requirements and features as option 1.

The only difference with option 1 is that you can choose a more specific configuration over the generic presets.

Requirements (Same as option 1):

- A device: Raspberry Pi, Desktop, Laptop or Android phone.
- Debrid account (for movies & series).
- (Optional) Trakt account.
- (Optional) Accounts for local regional media services.

Features (Same as option 1):

- All often-viewed movies & series instantly viewable.
- Obscure (colored gray) movies & series need to be downloaded first, the download progress is only visible from your debrid website.
- Watch Youtube, easily cast Youtube from your phone (Only for tv-connected installs).
- Watch local regional content (For example in Flanders we can watch most channels live and access most content aired in the last ~2 years in Kodi).
- Regular TV Remotes fully supported.
- Ability to view your own pictures/videos/music in kodi from local shares, cloud drives or files on your phone.

Installing goes like this:
- Generate a personalized install guide.
- Install kodi on your device.
- Follow your installation guide, this involves changing some settings in kodi and installing some addons.


## Option 3: Manually do an install with self-hosted web services

Requirements:

- Some basic technical skills (command line, home networks) (don't worry, everything is explained)
- A device that [LibreELEC supports](https://libreelec.tv/downloads/): Raspberry Pi, regular x86 mini PC, pretty much any single board computer.
- Debrid account (for movies & series).
- (Optional) Trakt account.
- (Optional) Accounts for local regional media services.
- (Optional) (Free) Domain pointing to your home ip & port forwarding tcp ports 80 and 433 to your device. (Only needed for access from oudside your home network)
- (Optional) Additional devices to integrate with this installation.

Features:

- All often-viewed movies & series instantly viewable.
- Obscure (colored gray) movies & series needs to be downloaded first, the download progress is viewable and managable from a self-hosted webpage (todo) (with the url viewable in Kodi).
- All movies & series watched in the last ~year can be streamed or downloaded for offline viewing on any device from a self-hosted webpage, with instructions in kodi.
- Watch Youtube, easily cast Youtube from your phone.
- Watch local regional content (For example in Flanders we can watch most channels live and access most content aired in the last ~2 years in Kodi).
- Regular TV Remotes fully supported.
- Ability to view your own pictures/videos/music in kodi from local shares, cloud drives or files on your phone.
- Installation guides and preset downloads (todo) available as self-hosted webpages, providing the same content on laptops/android phones for example.

Installing goes like this:

- Generate a personalized install guide and docker-services.zip.
- Install/Flash LibreELEC on your device.
- Connect to your device via ssh and copy-paste some commands, which sets up web services and a local kodi repo.
- Configure Kodi using the rest of the install manual (change some settings and install some addons)

## Explanations

### Debrid account

*Required for everything except regional content/youtube.*

A debrid service caches a large amount of torrents (and other sources) in the form of direct downloads.
To access this cache, typically, an account is needed which costs about 3€ a month, depending on the debrid service.

Supported debrid sercvices are:
  - AllDebrid